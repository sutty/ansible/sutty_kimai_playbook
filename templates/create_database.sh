#!/bin/sh
set -e

apk add --no-cache mariadb-client

database=$1; shift
username=$1; shift
password=$1; shift

mysql -e "create database if not exists ${database};"

for subnet in $@; do
  mysql -e "create user '${username}'@'${subnet}' identified by '${password}';"
  mysql -e "grant all privileges on ${database}.* to '${username}'@'${subnet}';"
done

mysql -e "flush privileges;"

apk del mariadb-client
