#!/bin/sh
set -e

cd $1

apk add --no-cache \
  composer \
  php82-pdo_mysql \
  php82-xsl \
  php82-xmlwriter \
  php82-pdo \
  php82-gd \
  php82-intl \
  php82-tokenizer \
  php82-xml \
  php82-ctype \
  php82-dom \
  php82-simplexml

composer install --optimize-autoloader -n

bin/console kimai:install -n

chown -R nobody:www-data .
chmod -R u=rX,g=rX,o= .
chmod -R u=rwX var
chmod +x bin/console

apk del composer
# Reinstall because composer takes some away
apk add --no-cache \
  php82-pdo_mysql \
  php82-xsl \
  php82-xmlwriter \
  php82-pdo \
  php82-gd \
  php82-intl \
  php82-tokenizer \
  php82-xml \
  php82-ctype \
  php82-dom \
  php82-simplexml
