Kimai Playbook
==============

Kimai Ansible playbook for Sutty.

Installation
------------

1. Either:
   * Get a copy of Sutty's inventory and host_vars repositories, or
   * Build your own.  The Kimai nodes need to be on the `sutty_kimai`
     inventory group.

2. Adapt `group_vars/sutty_kimai.yml`.
